#!/bin/bash

# name of layout file
layoutFile='zuba_prog'

#string in base.lst/evdev.lst
layoutLstString='  zuba_prog       Russian (Zubachev programmer)'

#these are to put to base.xml/evdev.xml
shortDesc='Zubachev programmer'
longDesc='Russian (Zubachev programmer)'
lang='rus' # iso639id

###################################

. shared/install
