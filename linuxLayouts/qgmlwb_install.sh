#!/bin/bash

# name of layout file
layoutFile='qgmlwb_prog'

#string in base.lst/evdev.lst
layoutLstString='  qgmlwb_prog     English (qgmlwb programmer)'

#these are to put to base.xml/evdev.xml
shortDesc='qgmlwb programmer'
longDesc='English (qgmlwb programmer)'
lang='eng' # iso639id

###################################

. shared/install
