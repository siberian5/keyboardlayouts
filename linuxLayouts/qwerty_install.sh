#!/bin/bash

# name of layout file
layoutFile='qwerty_prog'

#string in base.lst/evdev.lst
layoutLstString='  qwerty_prog     English (qwerty programmer)'

#these are to put to base.xml/evdev.xml
shortDesc='qwerty programmer'
longDesc='English (qwerty programmer)'
lang='eng' # iso639id

###################################

. shared/install
